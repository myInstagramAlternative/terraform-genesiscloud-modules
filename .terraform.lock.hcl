# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "hashicorp.com/edu/genesiscloud" {
  version = "0.0.3"
  hashes = [
    "h1:A48VToiYmZuLNyu5x4B5FM2HvO/XuQxis787oxo629U=",
  ]
}
